from datetime import datetime
from flask import Flask, render_template
from . import app
import re
import json

NAV_BAR_ITEMS = ['Home', 'Anagrams', 'About']


@app.route("/")
def home():
    return render_template("home.html", nav_bar_items=NAV_BAR_ITEMS, selected='Home')


@app.route("/about/")
def about():
    return render_template("about.html", nav_bar_items=NAV_BAR_ITEMS, selected='About')


@app.route("/anagrams/")
def anagrams():
    return render_template("anagrams.html", nav_bar_items=NAV_BAR_ITEMS, selected='Anagrams')


@app.route("/hello/")
@app.route("/hello/<name>")
def hello_there(name = ''):
    return render_template(
        "hello_there.html",
        name=re.sub('[^a-zA-Z]+', '', name),
        date=datetime.now()
    )


@app.route("/api/data")
def get_data():
    return app.send_static_file("data.json")


@app.route("/api/anagrams/<word>")
def get_anagrams(word = ''):
    word_in = word
    sorted_word = ''.join(sorted(word_in))

    with open('.\\static\\anagrams.json') as json_file:
        anagram_dict = json.load(json_file)

    anagrams = 'Anagrams for ' + word + ':<p>'
    for anagram in anagram_dict[sorted_word]:
        anagrams += anagram + '<p>'
    
    return anagrams
