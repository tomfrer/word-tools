import flask

app = flask.Flask(__name__)

# these are not used but leaving them here because they
# show how to create a global function


def get_nav_bar_list():
    nav_bar_list = ['Home', 'Anagrams', 'About']
    return nav_bar_list


app.jinja_env.globals.update(get_nav_bar_list=get_nav_bar_list)


def build_nav_bar(nav_bar_items, selected):
    nav_bar = ''
    for nav_bar_item in nav_bar_items:
        if nav_bar_item == selected:
            nav_bar += '<a href="{{ url_for( ' + nav_bar_item.lower() + ') }}" class="navbar-brand"> ' + nav_bar_item + ' </a>'
        else:
            nav_bar += '<a href="{{ url_for( ' + nav_bar_item.lower() + ') }}" class="navbar-item"> ' + nav_bar_item + ' </a>'
    return nav_bar


app.jinja_env.globals.update(build_nav_bar=build_nav_bar)
